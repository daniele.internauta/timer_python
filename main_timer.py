from tkinter import *
from tkinter import messagebox
import subprocess 
import datetime
class avvio():
    def __init__(self):

        self.r_p=Tk()
        self.r_p.geometry("250x80-5-45")
        self.r_p.iconbitmap("sveglia.ico")
        self.r_p.title("Spegni PC")
        self.r_p.resizable(False,False)

        titolo=Label(self.r_p,text="Imposta Timer Arresto",font=("Calibri 12 bold"),justify=CENTER,fg="blue")
        titolo.grid(row=0,column=0,columnspan=5)

        immagine= PhotoImage(file="sturtruppen.gif")
        img = Label(self.r_p,image=immagine)
        img.grid(row=0,column=6,rowspan=3)

        ore=Label(self.r_p,text="h")
        ore.grid(row=1,column=0)
        self.ore_i=Entry(self.r_p,width=2)
        self.ore_i.insert(0,"1")
        self.ore_i.grid(row=1,column=1)

        minuti=Label(self.r_p,text="min.")
        minuti.grid(row=1,column=2)
        self.minuti_i=Entry(self.r_p,width=2)
        self.minuti_i.insert(0,"1")
        self.minuti_i.grid(row=1,column=3)

        secondi=Label(self.r_p,text="sec.")
        secondi.grid(row=1,column=4)
        self.secondi_i=Entry(self.r_p,width=2)
        self.secondi_i.insert(0,"1")
        self.secondi_i.grid(row=1,column=5)

        self.cond_b1 = StringVar(value="Imposta")
        self.b1=Button(self.r_p,textvariable= self.cond_b1, command=lambda:self.Invia())
        self.b1.grid(row=2,column=0,columnspan=2)

        self.b2=Button(self.r_p,text="Annulla",command=lambda: self.Annulla())
        self.b2.grid(row=2,column=2,columnspan=2)

        self.r_p.mainloop()
    def Invia(self): 

        ore_p= self.ore_i.get()
        minuti_p=self.minuti_i.get()
        sec_p= self.secondi_i.get()

        if ore_p.isdigit() and minuti_p.isdigit() and sec_p.isdigit():  
            if int(ore_p) in range(0,721) and int(minuti_p) in range(0,61) and int(sec_p) in range(0,61): 

                ore_t = int(ore_p)*3600 
                minuti_t=int(minuti_p)*60 
                sec_t = int(sec_p) 
                self.tot_s = ore_t + minuti_t + sec_t  

                self.b1.destroy() 

                self.b1=Button(self.r_p,text='Avviato') 
                self.b1.grid(row=2,column=0,columnspan=2)

                self.conto=Label(self.r_p,text="",justify=LEFT) 
                self.conto.grid(row=2,column=3,columnspan=3) 

                self.Loop_1sec()
            else: 
                testo_av="Devi inserire l\'ora nel range da 0-720\n(che equivale a 30gg) i minuti tra 0-60\ne i secondi tra 0-60"
                messagebox.showwarning("Attenzione!",testo_av)
        else: 
            testo_av1="riempi tutti\ni campi\ncon dei numeri" 
            messagebox.showwarning("Attenzione!",testo_av1) 

    def Loop_1sec(self): 
        if self.tot_s > 0: 
            self.tot_s=self.tot_s-1 
            stamp=datetime.timedelta(seconds=self.tot_s) 
            self.conto.configure(text=stamp) 
            self.conto.after(1000,lambda: self.Loop_1sec()) 
        if self.tot_s == 0 :
            self.tot_s = -1
            self.b1.destroy()
            self.b1=Button(self.r_p,textvariable= self.cond_b1, command=lambda:self.Invia())
            self.b1.grid(row=2,column=0,columnspan=2)

            subprocess.Popen(['vlc','G:\\progetti_python\\file.mp4'])
                # subprocess.run(["shutdown","-s"])
                # subprocess.run(['shutdown -s'],shell=True)
    def Annulla(self): 
        if self.tot_s > 0:
            self.tot_s = -1 
            messagebox.showwarning("Attenzione!","Azione Annullata") 
            self.b1.destroy()
            self.b1 = Button(self.r_p,textvariable=self.cond_b1,command=lambda:self.Invia() )
            self.b1.grid(row=2,column=0,columnspan=2)
            


avvio()