Conto alla rovescia Python con interfaccia grafica
==================================================

Il programma da la possibilità di impostare un timer alla fine del quale eseguirà un comando ,
in questo caso lo shutdown e vlc (lo shutdown l'ho commentato lasciando che esegua solo VLC)

[![Senza-titolo-1.png](https://i.postimg.cc/C1FjShD5/Senza-titolo-1.png)](https://postimg.cc/sBNBJRTC)

Video commento (Scrittura codice):
----------------------------------
[BitTube](https://bit.tube/play?hash=QmcbGr3xid3iJqBH7mZwUzXjB8oMAXFfxin15F219B8WUd&channel=287658)

[Youtube](https://youtu.be/TpjXKQ-gbSw)  


Riferimenti delle Librerie Usate :
----------------------------------
[Tkinter](https://wiki.python.org/moin/TkInter)

[Subprocess](https://docs.python.org/2/library/subprocess.html)

[Datetime](https://docs.python.it/html/lib/datetime-datetime.html)